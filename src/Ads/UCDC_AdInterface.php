<?PHP

/**
 * This is the interface for all Ads. These Ads cannot be directly used as
 * SmartCodes. They must be wrapped in the UCDC_AdManager class.
 */
interface UCDC_AdInterface
{
  public function execute(object $subscriber) : string;
}