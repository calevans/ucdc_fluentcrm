<?php
/**
 * execute() returns a <a href=""><img src=""></a> string that is the current ad
 * for the associate dive center. If there is no dive center, it aborts.
 */
class UCDC_DiveCenterAd implements UCDC_SmartCodeInterface
{
  protected string $key = 'dive_center_ad';
  protected string $description = 'Dive Center Ad';

  public function execute(string $code, string $valueKey, string $defaultValue, object $subscriber) : string
  {
    $diveCenterSlug = $subscriber->custom_fields()['dive_center'] ?? '';
    return (new UCDC_DiveCenter( $diveCenterSlug ))->buildAd();
  }

 /**
  * Required by FluentCRM for all SmartCodes
  */
  public function getKey() : string
  {
    return $this->key;
  }

  public function getDescription() : string
  {
    return $this->description;
  }

}
