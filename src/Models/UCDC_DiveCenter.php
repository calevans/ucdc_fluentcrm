<?php

class UCDC_DiveCenter
{
  const DEFAULT_DIVE_CENTER ='eicc';

  protected array $diveCenter;

  public function __construct( protected string $diveCenterSlug )
  {
    $this->diveCenterId = $this->convertDiveCenterSlugToWPUserId( $this->diveCenterSlug );
  }

  public function buildAd() : string
  {
    /*
    <div style="text-align: center;">
<a href="https://unclecalsdiveclub.com/divecenter/{{contact.custom.dive_center|eicc}}-{{contact.custom.last_edition|100}}.html">
  <img style="
       padding:0;
       margin: auto;
       align:center;
       width: 100%;
       max-width: 640px;
       min-width:640px;
       min-height:200px;
              max-height:200px;"
       src="https://unclecalsdiveclub.com/divecenter//{{contact.custom.dive_center|eicc}}-{{contact.custom.last_edition|100}}.png" data-image="kmtrm7hsl306">
  </a>
</div>

    */
    return sprintf(
      '<div style="text-align: center; display: block; margin-left: auto; margin-right: auto;">
        <p><a href="%s"><img src="%s" style="padding:0; margin: auto; align:center; width: 100%%; max-width: 640px; min-width:640px; min-height:200px; max-height:200px;"></a></p>
      </div>',
      $this->getRedirectUrl(),
      $this->getImageFile()
    );
  }

  protected function getRedirectUrl() : string
  {
      $url = get_field( 'ad_url', 'user_' . $this->diveCenterId );
      return $url ?? '';
  }

  protected function getImageFile() : string
  {
      $image = get_field('ad_image', 'user_' . $this->diveCenterId);
      return wp_get_upload_dir()['baseurl'] . '/' . wp_get_attachment_metadata($image['ID'])['file'];
  }

  protected function convertDiveCenterSlugToWPUserId(string $diveCenter) : int
  {
    $wpdb = UCDC_WordPress::getWpdb();

    $userRecord = $wpdb->get_row($wpdb->prepare("select * from wp_usermeta where meta_key='dive_center' and meta_value =%s", $diveCenter), ARRAY_A);

    if ( is_null( $userRecord ) ) {
      return $this->convertDiveCenterSlugToWPUserId( self::DEFAULT_DIVE_CENTER );
    }

    return (int)$userRecord['user_id'];
  }

}

