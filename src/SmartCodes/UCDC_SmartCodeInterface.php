<?php
interface UCDC_SmartCodeInterface
{
  public function execute(string $code, string $valueKey, string $defaultValue, object $subscriber) : string;
  public function getKey() : string;
  public function getDescription() : string;
}