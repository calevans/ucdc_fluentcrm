<?php

abstract class UCDC_AbstractAd implements UCDC_AdInterface
{

  public function __construct(protected string $productSlug, protected string $boughtTag){}

  public function execute(object $subscriber) :string
  {
    if ( $this->previouslyBoughThisProduct( $subscriber ) ) {
      return '';
    }

    $product = get_page_by_path($this->productSlug, OBJECT, 'product');

    if (empty($product)) {
      return '';
    }

    $productTitle = $product->post_title;
    $productLink = get_permalink($product->ID);
    $productImage = get_the_post_thumbnail_url($product->ID, 'thumbnail');
    $productMetaDesc = get_post_meta($product->ID, '_yoast_wpseo_metadesc', true);
    return $this->getCard($productTitle, $productLink, $productImage, $productMetaDesc);
  }

  /**
   * @todo: This is ugly. Make a method that returns the card that can be
   * overridden for each product.
   */
  protected function getCard(string $productTitle, string $productLink, string $productImage, string $productMetaDesc = '') : string
  {
    return sprintf(
      '<div style="text-align: center; display: block; margin-left: auto; margin-right: auto;">
        <p style="text-align:center; font-size: cal(1em +.20vw); font-weight:bold;">%s</p>
        <p><a href="%s"><img src="%s" style="display: block; margin: 0 auto;"></a></p>
        <p>%s</p>
      </div>',
      $productTitle,
      $productLink,
      $productImage,
      $productMetaDesc
    );
  }

  protected function previouslyBoughThisProduct( object $subscriber ) : bool
  {
    foreach($subscriber->tags()->get() as $tag) {
      if ( $tag->slug === $this->boughtTag) {
        return true;
      }
    }

    return false;
  }

  public function getproductSlug() : string
  {
    return $this->productSlug;
  }
}
