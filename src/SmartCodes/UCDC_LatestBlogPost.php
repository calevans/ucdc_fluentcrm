<?php
/**
 * This goes in the footer of each email.
 *
 * execute() returns a <a href=""><img src=""></a> string that is the current ad
 * for the associate dive center. If there is no dive center, it aborts.
 */
class UCDC_LatestBlogPost implements UCDC_SmartCodeInterface
{
  protected string $key = 'latest_blog_post';
  protected string $description = 'Latest Blog Post';

  public function execute(string $code, string $valueKey, string $defaultValue, object $subscriber) : string
  {
    $catId = UCDC_WordPress::getOrCreateCategory('New Scuba Diver Advice');

    $blog = get_posts([
      'post_type' => 'post',
      'posts_per_page' => 1,
      'orderby' => 'date',
      'order' => 'DESC',
      'post_status' => 'publish',
      'cat' => $catId,
    ]);

    if (empty($blog)) {
      return $defaultValue??'';
    }

    $blogPost = $blog[0];

    $blogTitle = $blogPost->post_title;
    $blogLink = get_permalink($blogPost->ID);
    $blogImage = get_the_post_thumbnail_url($blogPost->ID, 'medium');
    $blogMetaDesc = get_post_meta($blogPost->ID, '_yoast_wpseo_metadesc', true);

    return $this->buildOutput($blogLink, $blogImage, $blogTitle, $blogMetaDesc);
  }

  protected function buildOutput(string $blogLink, string $blogImage, string $blogTitle, string $blogMetaDesc) : string
  {
    return sprintf(
      '<div style="padding:20px; border:solid 1px #000; border-radius:10px;">
      <p style="text-align:center; font-size: cal(1em +.20vw); font-weight:bold;">Our Latest Blog Post:</p>
        <table style="width: 70%% text-align:center;">
          <tr>
            <td align="center">
              <a href="%s"><img src="%s" style="margin:10px auto;"></a>
            </td>
          </tr>
          <tr>
            <td>
              <p style="font-size: calc(1em + 0.10vw); text-align: center; padding:10px;"><a href="%s">%s</a></p>
            </td>
          </tr>
          <tr>
            <td>
              <span style="font-size: calc(0.8em + 0.05vw);">%s</span>
            </td>
          </tr>
        </table>
        </div>',
      $blogLink ?? '',
      $blogImage ?? '',
      $blogLink ?? '',
      $blogTitle ?? '',
      $blogMetaDesc ?? ''
    );
  }

  public function getKey() : string
  {
    return $this->key;
  }

  public function getDescription() : string
  {
    return $this->description;
  }

}
