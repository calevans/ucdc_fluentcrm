<?php

/**
 * NOT CURRENTLY IN USE!
 * NOT CURRENTLY IN USE!
 * NOT CURRENTLY IN USE!
 * NOT CURRENTLY IN USE!
 * NOT CURRENTLY IN USE!
 * NOT CURRENTLY IN USE!
 *
 * Display an ad for the next published meeting.
 *
 * UCDC has monthly meetings. In eahc email I want an ad to be displayed for
 * the next upcoming meeting that the user has not already registered for. If
 * the user has registered for meeting #1, then don't show them that ad, show
 * them the ad for meeting #2.
 *
 * If there are no more published meetings, use the default vaule which should
 * normally be something like a single pixel jpg. If the default value is used
 * then it will not be wrapped in an A tag.
 */
class UCDC_MeetingAd implements UCDC_SmartCodeInterface
{
  protected string $key = 'meeting_ad';
  protected string $description = 'Meeting Ad';

  /**
   * Main Method
   *
   * UCDC has a directory for images in wp-content/banners. default.jog, a
   * single pixel image used as the default value resides there.
   *
   * In most cases, I put default.jpg as my default value. However, it can be
   * any graphic that exists in wp-content/banners.
   */
  public function execute(string $code, string $valueKey, string $defaultValue, object $subscriber) : string
  {
    $subscribedMeetings = $this->getSubscribedMeetings( $subscriber );
    $allMeetings = $this->getPublishedMeetings();
    $hit = $this->findNextMeeting( $allMeetings, $subscribedMeetings );

    if ( empty( $hit ) ) {
      return sprintf(
        '<img src="%s" />',
        wp_get_upload_dir()['baseurl'] . '/banners/' . $defaultValue
      );
    }

    $permaLink = get_post_permalink( $hit['id'] );

    if (! $permaLink ) {
      $permaLink = '';
    }

    return sprintf(
      '<a href="%s" ><img src="%s" /></a>',
      $permaLink,
      get_field( 'email_ad_image', $hit['id'] ) ?? (wp_get_upload_dir()['baseurl'] . '/banners/' . $defaultValue)
    );

  }

  /**
   * Build an array of the meetings for which  the passed in user has already
   * registered.
   */
  protected function getSubscribedMeetings( object $subscriber ) : array
  {
    $meetings = [];
    foreach($subscriber->tags()->get() as $tag) {
      if (substr( $tag->slug,0,8 ) !== 'meeting_') {
        continue;
      }
      $postId = explode( '_', $tag->slug )[1];

      $meetings[ $postId ] = [
        'id' => $postId,
        'slug' => $tag->slug,
        'description' => $tag->description,
      ];
    }
    return $meetings;
  }

  /**
   * Build an array of the WP Posts in the category of meeting whose
   * meeting_date is greater than today.
   *
   * This array is sorted by meetings date/time
   */
  protected function getPublishedMeetings() : array
  {
    $args = [ 'category_name' => 'meetings' ];
    $returnValue = [];
    $the_query = new WP_Query( $args );
    foreach($the_query->get_posts() as $meeting) {
      $meetingDate = new \DateTimeImmutable( get_field('meeting_date', $meeting->ID ) . ' ' . get_field('meeting_time', $meeting->ID ) );
      $today = new \DateTimeImmutable();

      if ($meetingDate < $today ) {
        continue;
      }

      $returnValue[$meeting->ID] = [
        'id' => $meeting->ID,
        'title' => $meeting->title,
        'date' => $meetingDate,
        'time' => get_field('meeting_time', $meeting->ID )??'',
        'speaker' => get_field('speaker_name', $meeting->ID )??'',
      ];

    }

    usort(
      $returnValue,
      function ($a,$b) {
        return $a['date'] <=> $b['date'];
      }
    );

    return $returnValue;
  }

  /**
   * Given a list of all meetings and a lit of meetings this user is registered
   * for, it returns the next meeting that this user has not registered for. If
   * the user has registered for all meetings, it returns an empty array.
   */
  protected function findNextMeeting(array $allMeetings, array $subscribedMeetings ) : array
  {
    foreach( $allMeetings as $meeting ) {

      $filteredMeetings = array_filter(
        $subscribedMeetings,
        function ($y) use ($meeting) {return (int)$y['id'] === (int)$meeting['id'];}
      );

      /*
       * If the above array_filter() finds a hit (count()>0) then we keep moving
       * because the user is already registered for this one. Otherwise, we've
       * got a winner and we return.
       */
      if ( count($filteredMeetings) === 0 ) {
        return $meeting;
      }
    }

    return [];

  }

  public function getKey() : string
  {
    return $this->key;
  }

  public function getDescription() : string
  {
    return $this->description;
  }

}
