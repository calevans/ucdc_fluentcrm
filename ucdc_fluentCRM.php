<?php
/**
 * Plugin Name:     UCDC->FluentCRM Integrations
 * Plugin URI:      https://unclecalsdiveclub.com
 * Description:     All the integrations UCDC needs to work with FLuentCRM
 * Author:          Cal Evans
 * Author URI:      https://calevans.com
 * Text Domain:     ucdc-fluent-crm
 * Domain Path:     /languages
 * Version:         2.0.1
 *
 * @package         Ucdc_FluentCRM
 */

if (!defined('ABSPATH')) die; // If this file is called directly, abort.

$dir = dirname(__FILE__) . '/';

/**
 * @todo Remove the hard coded ads and figure out how to tag them in
 *       WooCommerce.
 *
 */

/**
 * Include all the files we need to make this happen
 *
 * These have to come first because others depend on them.
 */
require_once $dir . 'src/SmartCodes/UCDC_SmartCodeInterface.php';
require_once $dir . 'src/Actions/UCDC_Action_Interface.php';
require_once $dir . 'src/Ads/UCDC_AdInterface.php';
require_once $dir . 'src/Ads/UCDC_AbstractAd.php';


/*
 * Include the Models
 */
require_once $dir . 'src/Models/UCDC_WordPress.php';
require_once $dir . 'src/Models/UCDC_DiveCenter.php';

/*
 * Include the Actions
 */
require_once $dir . 'src/Actions/UCDC_AddMemberToMeeting.php'; // don't need this anymore

/*
 * Include the Hooks
 */
require_once $dir . 'src/Hooks/UCDC_EmailOpenedHook.php';

/*
 * Include the SmartCodes
 */
require_once $dir . 'src/SmartCodes/UCDC_CacheBuster.php';
require_once $dir . 'src/SmartCodes/UCDC_DiveCenterAd.php';
require_once $dir . 'src/SmartCodes/UCDC_EmailFooterAdManager.php';
require_once $dir . 'src/SmartCodes/UCDC_LatestBlogPost.php';
require_once $dir . 'src/SmartCodes/UCDC_MeetingAd.php';
require_once $dir . 'src/SmartCodes/UCDC_PersonalizedMugImage.php';

/*
 * Include the Ads
 */
require_once $dir . 'src/Ads/UCDC_GenericPersonalizedMugAd.php';
require_once $dir . 'src/Ads/UCDC_GenericProductAd.php';

/*
 * Include the base class
 */
require_once $dir . 'src/UCDC_FluentCRM.php';

/*
 * Instantiate the main object
 */
$ucdcFc = new UCDC_FluentCRM();

/*
 * Register all the SmartCodes.
 * This has to be done BEFORE calling init().
 */
//$ucdcFc->registerAction((new UCDC_AddMemberToMeeting()));
//$ucdcFc->registerSmartCode(( new UCDC_MeetingAd() ));
$ucdcFc->registerSmartCode(( new UCDC_CacheBuster() ));
$ucdcFc->registerSmartCode(( new UCDC_LatestBlogPost() ));
$ucdcFc->registerSmartCode(( new UCDC_DiveCenterAd() ));
//$ucdcFc->registerSmartCode(( new UCDC_PersonalizedMugImage() )); // Not sure I need this anymore.

/**
 * Implement the new Email Footer Ad Manager. Anything added here will be used
 * to build the ad at the bottom of each email that has the code.
 *
 * See the Readme for full instructions.
 *
 * @todo Remembering to add this code is a pain. it would be great if we could
 *       just tag the product and have the AdManager pull the list of tagged
 *       products. Maybe just add a tag 'email-ad' and then pull all the
 *       product with that tag and select one.
 */
$efAdManager = new UCDC_EmailFooterAdManager();
//$efAdManager->addAd( (new UCDC_GenericPersonalizedMugAd( '-dives-well-with-others','bought-dives-well-with-others-mug' ) ) );
$efAdManager->addAd( (new UCDC_GenericProductAd( 'pressure-what-pressure-t-shirt','bought-pressure-what-pressure-t-shirt' ) ) );
$efAdManager->addAd( (new UCDC_GenericProductAd( 'enriched-brew-coffee-mug','bought-enriched-brew-mug' ) ) );
$efAdManager->addAd( (new UCDC_GenericProductAd( 'dive-confidently-turtle-t-shirt','bought-dive-confidently-turtle-t-shirt' ) ) );
$efAdManager->addAd( (new UCDC_GenericProductAd( 'majestic-ray-t-shirt','bought-majestic-ray-t-shirt' ) ) );
$efAdManager->addAd( (new UCDC_GenericProductAd( 'scuba-t-shirt','bought-scuba-t-shirt' ) ) );

$efAdManager->setRandomize( true );
$ucdcFc->registerSmartCode( $efAdManager );

/*
 * Register all the SmartCodes.
 *
 * See the Readme for full instructions.
 */
$ucdcFc->registerHook((new UCDC_EmailOpenedHook('book', 'book-promo-opened')));
$ucdcFc->registerHook((new UCDC_EmailOpenedHook('ebmug', 'enriched-brew-mug-promo-opened')));
$ucdcFc->registerHook((new UCDC_EmailOpenedHook('s2s', 'stream-2-sea-promo-opened')));
$ucdcFc->registerHook((new UCDC_EmailOpenedHook('dcTurtleT', 'email-dive-confidently-turtle-t-shirt-opened')));
$ucdcFc->registerHook((new UCDC_EmailOpenedHook('magrayt', 'email-majestic-ray-t-shirt-opened')));
$ucdcFc->registerHook((new UCDC_EmailOpenedHook('sng', 'email-scuba-news-gazette-opened')));
$ucdcFc->registerHook((new UCDC_EmailOpenedHook('scubat', 'email-scuba-t-opened')));
$ucdcFc->registerHook((new UCDC_EmailOpenedHook('ecbm', 'enriched-brew-mug-promo-opened')));


/*
 * Kick everything off
 */
$ucdcFc->init();
