<?php

class UCDC_WordPress
{
  static public function getOrCreateCategory(string $categoryName): int
  {
    // Check if the category exists
    $category = term_exists($categoryName, 'category');

    // If the category does not exist, create it
    if ($category === 0 || $category === null) {
      $category = \wp_insert_category(array('cat_name' => $categoryName));
    } else {
      // If the category exists, get its ID
      $category = $category['term_id'];
    }
    return (int) $category;
  }
  /**
   * This only exisats because I hate Globals.
   */
  static public function getWpdb()
  {
    global $wpdb;
    return $wpdb;
  }


}
