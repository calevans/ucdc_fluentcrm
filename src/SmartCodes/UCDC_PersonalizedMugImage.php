<?php

/**
 * Right now we are doing personalized mugs as a people join the list.
 * THe URL is always FIRST_NAME-dives-well-with-others. We use Printful to
 * create the mugs. This Smartcode will look for a post with the correct
 * slug. If it finds one, it will return the featured image for the post so we
 * can use it in the email.
 *
 */
class UCDC_PersonalizedMugImage implements UCDC_SmartCodeInterface
{
  protected string $key = 'personalized_mug_image';
  protected string $description = 'Personalized Mug Image';

  /**
   * Main Method
   */
  public function execute(string $code, string $valueKey, string $defaultValue, object $subscriber) : string
  {

    $post = get_page_by_path(strtolower($subscriber->first_name) . '-dives-well-with-others', OBJECT, 'product');

    if (empty($post)) {
      return $defaultValue;
    }

    $postTitle = $post->post_title;
    $postLink = get_permalink($post->ID);
    $postImage = get_the_post_thumbnail_url($post->ID, 'medium');

    $card = sprintf(
      '<div style="text-align: center; display: block; margin-left: auto; margin-right: auto;">
        <p><a href="%s"><img src="%s" style="display: block; margin: 0 auto;"></a></p>
      </div>',
      $postLink,
      $postImage,
      $postTitle,
      $postTitle
    );

    return $card;
  }



  public function getKey() : string
  {
    return $this->key;
  }

  public function getDescription() : string
  {
    return $this->description;
  }


}
