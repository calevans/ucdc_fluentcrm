<?php

/**
 * Email Footer Ad Manager
 *
 * This SmartCode will spin through all the ads it has been told about and do 1
 * of 2 things.
 *
 * 1) Iterate over the list sequentially until one of them returns a non-empty
 *    string.
 * 2) Randomly iterate over the list until one of them returns a non-empty
 *    string.
 *
 * If none of them return a non-empty string, then it will return the default
 * value.
 *
 * @todo break out some of the CSS into variables that can be set as parameters.
 */
class UCDC_EmailFooterAdManager implements UCDC_SmartCodeInterface
{
  protected string $key = 'email_footer_ad_manager';
  protected string $description = 'Email Footer Ad Manager';
  protected array $ads = [];
  protected bool $randomize = false;
  protected string $preTag = '<div style="margin: auto; width: 70%; padding:20px; border:solid 1px #000; border-radius:10px;">';
  protected string $postTag = '</div>';

  public function execute( string $code, string $valueKey, string $defaultValue, object $subscriber ) : string
  {

    if ( $this->randomize ) {
      $returnValue = $this->pickRandomAd($defaultValue, $subscriber);
      return $this->preTag . $returnValue . $this->postTag;
    }

    $returnValue = $this->pickSequentialAd( $defaultValue, $subscriber );
    return $this->preTag . $returnValue . $this->postTag;

  }

  public function addAd( UCDC_AdInterface $ad ) : void
  {
    $this->ads[] = $ad;
  }

  protected function pickSequentialAd( string $defaultValue, object $subscriber ) : string
  {
    foreach ($this->ads as $ad) {
      $returnValue = $ad->execute($subscriber);
      if ( ! empty( $returnValue ) ) {
        return $returnValue;
      }
    }
    return $defaultValue;
  }

  protected function pickRandomAd( string $defaultValue, object $subscriber ) : string
  {
    $ads = $this->ads;
    shuffle( $ads );
    foreach ( $ads as $ad ) {
      $returnValue = $ad->execute( $subscriber );
      if ( ! empty( $returnValue ) ) {
        return $returnValue;
      }
    }
    return $defaultValue;
  }

  public function setRandomize( bool $randomize ) : void
  {
    $this->randomize = $randomize;
  }

  /**
   * Required by FluentCRM
   */
  public function getKey() : string
  {
    return $this->key;
  }

  /**
   * Required by FluentCRM
   */
  public function getDescription() : string
  {
    return $this->description;
  }


}
