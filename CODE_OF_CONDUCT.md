# Code of Conduct

"Be excellent to each other. "<br>
  -- Bill S. Preston, Esq.

Access to this repo and it's code is a privilege, not a right. A privilege I am happy to revoke when and if I feel like it.

This project is about code. If you have code to contribute,or you wish to discuss code, I'm all ears. Anything else, take it somewhere more appropriate.

As the BDFL, I am the final arbiter of all disputes, including anyone disputing this policy.

Cheers!<br />
=C=

Cal Evans
cal@calevans.com