<?php
use FluentCrm\App\Models\CampaignEmail;
use FluentCrm\App\Models\Campaign;
use FluentCrm\App\Models\Subscriber;
use FluentCrm\App\Models\Tag;

/**
 * Abstract class that is the base class for all email opened hooks.
 */

class UCDC_EmailOpenedHook
{

  /**
   * The name of the hook to hook into. In the case of Email Opened, it is
   * almost always this one.
   */
  protected string $hookName = 'fluent_crm/email_opened';

  /**
   * The slug for the campaign we are reacting to.
   */
  protected string $slug;

  /**
   * The tag to add to the subscriber.
   */
  protected string $tagToAdd;

  /*
   * The priority to assign.
   */
  protected int $priority = 20;

  public function __construct(string $slug, string $tagToAdd, int $priority = 20)
  {
    $this->slug = trim(strtolower($slug));
    $this->tagToAdd = $tagToAdd;
    $this->priority = $priority;
  }

  public function init() : void
  {
    add_action( $this->hookName, [ $this, 'handle' ], $this->priority );
  }

  /**
   * Deal with the fact that an email has been opened
   */
  /**
   * NOTE: To use this you have to add the tag into the system first.
   */
  public function handle(object $email): void
  {

    if (is_null($email)) {
      return;
    }

    $subscriber = Subscriber::where('email', $email->email_address)->first();
    $hasTag = false;

    foreach ($subscriber->tags()->get() as $tag) {

      if ($tag->slug === $this->tagToAdd) {
        $hasTag = true;
        break;
      }
    }

    if ($hasTag === false) {
      $thisTag = Tag::where('slug', $this->tagToAdd)->first();
      $subscriber->attachTags([$thisTag->id]);
    }
  }

  /**
   * Convert a hash to an email object.
   * @deprecated. We won't need this anymore because do_cation() passes in the
   * CampaignEmail object.
   */

  protected function convertHashToEmail ( string $hash) : object
  {
    return CampaignEmail::where( 'email_hash', $hash )->first();
  }

  /**
   * Given an email hash, this method will:
   * - fetch the email
   * - check to make sure it is one we want
   *
   * If either of those fail, it returns null.
   * Otherwise it returns the email object
   */
  protected function fetchEmail(string $mailHash): ?object
  {
    $email = $this->convertHashToEmail($mailHash);

    if (is_null($email)) {
      return null;
    }

    $campaign = Campaign::where('id', $email->campaign_id)->first();
    if (empty($campaign)) {
      return null;
    }
    /*
     * If the slug is at the beginning of the title, we want to process it.
     */
    $found = false;
    foreach (explode(',', $campaign->title) as $part) {
      if (strtolower(trim($part)) === $this->slug) {
        $found = true;
        break;
      }
    }
    /*
    $parts = explode(',', $campaign->title);
    if (count($parts) === 1 || strtolower(trim($parts[0])) !== $this->slug) {
      return null;
    }
    */
    if ($found === false) {
      return null;
    }

    return $email;
  }
}
