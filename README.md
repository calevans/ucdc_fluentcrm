# Uncle Cal's Dive Club FluentCRM Connector

This repo contains all the custom code necessary to connect UCDC to FluentCRM.

**IMPORTANT!**

## FluentCRM

For email open tags to work, you need to make sure that we've modified `fluent-crm/app/Hooks/Handlers/ExternalPages.php`. In the method `trackEmailOpen()` add the line:

```php
do_action('fluent_crm/email_opened', $mailHash);
```

Add it directly after:

```php
CampaignUrlMetric::maybeInsert([
    'type'          => 'open',
    'campaign_id'   => $email->campaign_id,
    'subscriber_id' => $email->subscriber_id,
    'ip_address'    => FluentCrm()->request->getIp(fluentCrmWillAnonymizeIp())
]);
```

This gives us the hook that this needs to make everything else work.

## WooCommerce

`woocommerce/includes/admin/class-wc-admin.php` has to be patched.

Look for

```php
public function prevent_admin_access().
```

Then find the line

```php
$exempted_paths = array( 'admin-post.php', 'admin-ajax.php', 'async-upload.php' );
```

Make sure that `'async-upload.php'` is in the array.


## How to add a tag to a contact when an email is opened

To add a tag to a contact when they open an email, do the following:

## Create a new tag in FluentCRM

DO this first because you need the tag's slug.

## Figure out what your slug will be

It should be something like `ebmug` for the Enhanced Brew mug. It should be unique and not conflict with any other slugs. Make it short because it has to go at the beginning of the email campaign name. `ecbmug, GET YOUR ENHANCED BREW MUG TODAY!`. NOTE: IT does not go at the beginning of the SUBJECT LINE, it goes at the beginning of the campaign name.

## Add a line to `public/wp-content/plugins/ucdc_fluentCRM/ucdc_fluentCRM.php`

```php
$ucdcFc->registerHook((new UCDC_EmailOpenedHook(SLUG, TAG-SLUG)));
```

## Optional

If you want to expire the tags after a certain amount of time, you need to add an automation.<br /><br />
Step 0 The trigger is the tag you created being set on an account.<br />
Step 1 is to wait X time units (30 days?)<br />
Step 2 is a Remove From Tag. Select the tag you created in step 1.<br />
Step 3 is End this Funnel Here.<br />

## EMailFooterAdManager

This class is used to manage the ads that go in the footer of emails.

In the email just put `{{ucdc_smartcodes.email_footer_ad_manager}}` in a raw HTML container. The EMailFooterAdManager wraps the ad in a div tag and you can control the style by modifying the class.

When you create a new product you have to add it to `ucdc_FluentCRM.php` to get it to start in the rotation. You add a line that looks like this:

```php
$efAdManager->addAd( (new UCDC_GenericProductAd( 'majestic-ray-t-shirt','bought-majestic-ray-t-shirt' ) ) );
```
The first parameter is the slug of the product. The second parameter is the tag that is set when the product is purchased.
 The slug of the product is the part of the URL that appears after `product`. For example, the URL for the Majestic Ray T-Shirt is `https://unclecalsdiveclub.com/product/majestic-ray-t-shirt/`. The slug is `majestic-ray-t-shirt`.

Next, add the tag to FluentCRM. Go to FluentCRM -> Tags and add a new tag called `bought-majestic-ray-t-shirt`. FluentCRM is dumb, so you can't just paste the slug in, it won't find it. So start typing the title you gave the tag, it'll find it. Click on it and save.

Finally, edit the product. Go to the FluentCRM tab and add the tag `bought-majestic-ray-t-shirt` to the product. That way the ad won't be shown if they have already purchased.


## Latest BLog Post

Put `{{ucdc_smartcodes.latest_blog_post}}` in a raw HTML container.

## Related documentation

- https://fluentcrm.com/docs-category/devloper/
- https://developers.fluentcrm.com/
