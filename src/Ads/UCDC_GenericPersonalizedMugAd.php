<?php
/**
 * Displays an ad for a person's personalized mug if they have not yet
 * purchased it.
 *
 */
class UCDC_GenericPersonalizedMugAd extends UCDC_AbstractAd
{

  public function execute(object $subscriber) : string
  {

    if ( $this->previouslyBoughThisProduct( $subscriber ) ) {
      return '';
    }

    /*
     * Not everybody has a personalized mug. If we've not created one for this
     * name, return an empty string.
     */
    $product = get_page_by_path(strtolower($subscriber->first_name) . $this->productSlug, OBJECT, 'product');

    if (empty($product)) {
      return '';
    }

    $productTitle = $product->post_title;
    $productLink = get_permalink($product->ID);
    $productImage = get_the_post_thumbnail_url($product->ID, 'thumbnail');
    $productMetaDesc = get_post_meta($product->ID, '_yoast_wpseo_metadesc', true);
    $productMetaDesc = 'Description';
    return $this->getCard($productTitle, $productLink, $productImage,$productMetaDesc);
  }

  protected function getCard(string $productTitle, string $productLink, string $productImage, string $productMetaDesc = '') : string
  {
    return sprintf(
      '<div style="text-align: center; padding:5%%;">
        <p style="text-align:center; font-size: cal(1em +.20vw); font-weight:bold;">Get your personalized mug today!<br /><br /></p>
        <p><a href="%s"><img src="%s" style="display: block; margin: 0 auto;"></a></p>
        <p><a href="%s">Get yours today!</a></p>
      </div>',
      $productLink,
      $productImage,
      $productLink
    );
  }
}