<?php

/**
 * This is a Fluent CRM Action to add a properly tagged mailing list member to
 * a meeting. It is triggered from an automation that sees that a member has the
 * MEETING tag added. It looks at the member's record and pulls the field
 * "last_meeting_registered". If it has a value, it adds that tag to the member.
 * If the tag meeting_postId does not exist, it creates it first.
 */

use FluentCrm\App\Models\Tag;
use FluentCrm\App\Services\Funnel\BaseAction;

class UCDC_AddMemberToMeeting extends BaseAction
{
    const META_KEY = 'last_meeting_registered';
    const MEETINGS_META_KEY = 'meeting_registered';
    const BASE_TAG_NAME = 'meeting_%d';

    public function __construct()
    {
        $this->actionName = 'register_member_for_meeting';
        $this->priority=20;
		parent::__construct();
    }

    /*
     * NECESSARY
     */
    public function pushBlock($blocks, $funnel)
    {
        $this->funnel = $funnel;

        $block = $this->getBlock();
        if($block) {
            $block['type'] = 'action';
            $blocks[$this->actionName] = $block;
        }

        return $blocks;
    }

    /*
     * NECESSARY
     */
    public function getBlock()
    {
        return [
            'category'    => "Uncle Cal's Dive Club",
            'title'       => 'Add Member to a Meeting',
            'description' => 'Add a mailing list member to a meeting',
            'icon'        => 'fc-icon-writing',
            'settings'    => []
        ];
    }

    /*
     * NECESSARY
     */
    public function pushBlockFields($fields, $funnel)
    {
        $this->funnel = $funnel;
        $fields[$this->actionName] = $this->getBlockFields();
        return $fields;
    }

    /*
     * NECESSARY
     */
    public function getBlockFields()
    {
        return [];
    }



    /**
     * This is the main method
     */
    public function handle( $subscriber, $sequence, $funnelSubscriberId, $funnelMetric )
    {
      $meetingNo = fluentcrm_get_subscriber_meta( $subscriber->id, self::META_KEY );
      $tagToSet = sprintf( self::BASE_TAG_NAME, $meetingNo );
      $tag = $this->getMeetingTag( $tagToSet );
      $subscriber->attachTags( [ (int)$tag->id ] );
    }

    /**
     * Get the tag for the meeting.
     */
    protected function getMeetingTag( string $tagName )
    {
        $tagApi = FluentCrmApi('tags');
        $tag = $tagApi->get()->where('slug',$tagName)->first();
        if (! is_null( $tag ) ) {
            return $tag;
        }

        return $this->createMeetingTag( $tagName );
    }

    /**
     * If the needed tag does not exist, create it.
     */
    protected function createMeetingTag( string $tagName )
    {
        $tagSlug = strtr( strtolower( $tagName ),[' '=>'-',"'"=>"",'"'=>''] );
        $postId = explode( '_', $tagName )[1];

        $post = get_post( $postId );

        $tag = Tag::create([
            'title' => 'Meeting : ' . $post->post_title,
            'slug'  => $tagSlug,
            'description' => 'UCDC Meeting ('. $postId .') - ' . $post->post_title
        ]);

        do_action( 'fluentcrm_tag_created', $tag->id );
        do_action( 'fluent_crm/tag_created', $tag );

        return $tag;
    }
}
