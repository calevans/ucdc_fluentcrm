<?php
/**
 * This is the ad that goes at the footer of each email. If there is not a footer.png
 *
 * execute() returns a <a href=""><img src=""></a> string that is the current ad
 * for the associate dive center. If there is no dive center, it aborts.
 */
class UCDC_CacheBuster implements UCDC_SmartCodeInterface
{
  protected string $key = 'cache_buster';
  protected string $description = 'Cache Buster';

  public function execute(string $code, string $valueKey, string $defaultValue, object $subscriber) : string
  {
    return strtolower(substr(base64_encode(date('U')),0,-2));
  }

  public function getKey() : string
  {
    return $this->key;
  }

  public function getDescription() : string
  {
    return $this->description;
  }

}
