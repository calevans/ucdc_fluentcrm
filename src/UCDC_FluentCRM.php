<?php

/**
 * Marshaler class for all the UCDC code that interacts with FluentCRM
 */
class UCDC_FluentCRM
{
  protected array $actions = [];
  protected array $hooks = [];
  protected array $smartCodes = [];
  protected string $smartCodeKey = 'ucdc_smartcodes';
  protected string $smartCodeTitle = 'UCDC';

  /**
   * Initialize everything. This is called during WP's INIT action.
   */
  public function init() :void
  {
    foreach ($this->actions as $action) {
      add_action('fluentcrm_loaded', function ($action) {
        $action;
      });
    }

    add_action('fluentcrm_loaded', [ $this,'initSmartCodes' ]);
    add_action('fluentcrm_loaded', [ $this,'initHooks' ]);
  }

  /*
   * Unlike actions, SmartCodes cannot be registered until FluentCRM is loaded.
   * ALso, you have to register them all at once.
   */
  public function initSmartCodes() : void
  {
    $smartCodeList = [];
    foreach ($this->smartCodes as $smartCode) {
      $smartCodeList[ $smartCode->getKey() ] = $smartCode->getDescription();
    }

    FluentCrmApi('extender')->addSmartCode(
      $this->smartCodeKey,
      $this->smartCodeTitle,
      $smartCodeList,
      [ $this, 'executeSmartCode' ]
    );
  }

  public function registerSmartCode( UCDC_SmartCodeInterface $smartCode ) : void
  {
    $this->smartCodes[ $smartCode->getKey() ] = $smartCode;
  }

  public function registerAction( $action ) : void
  {
    $this->actions[] = $action;
  }

  /*
   * Register WordPress actions we want to hook into. This is only for actions.
   * Filters have to be dealt with where they are needed.
   *
   * All actions regstered through this system will be initalized after
   * fluentcrm has been initalialized. Therefore all the parts of fluentcrm are
   * available for use.
   *
   */
  public function initHooks() : void
  {
    foreach ($this->hooks as $hook) {
      $hook->init();
    }
  }

  public function registerHook(object $object) : void
  {
    $this->hooks[] = $object;
  }

  /**
   * The return value is unpredictable. I feel sure it's almost always a string
   * but we really don't know what $defaultValue is. I would love to typehint
   * both this and the execute() methods of each SmartCode object as a string.
   */
  public function executeSmartCode( $code, $valueKey, $defaultValue, $subscriber )
  {
    if ( ! isset( $this->smartCodes[ $valueKey ] ) ) {
      return $defaultValue;
    }

    return $this->smartCodes[ $valueKey ]->execute( $code, $valueKey, $defaultValue, $subscriber );
  }

}